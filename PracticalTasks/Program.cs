﻿using System;
using PracticalTasks.Arrays;
using PracticalTasks.Conditionals;
using PracticalTasks.Logic;

namespace PracticalTasks
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //ArrayTest();

            //ConditionalsTest();

            LogicTest();

            Console.ReadKey();
        }

        private static void LogicTest()
        {
            //GreatPartyTest();

            CanHazTableTest();
        }

        private static void CanHazTableTest()
        {
            CanHazTable table = new CanHazTable();
            table.Run();
        }

        private static void GreatPartyTest()
        {
            GreatParty greatParty = new GreatParty();
            greatParty.Run();
        }

        private static void ConditionalsTest()
        {
            // InTroubleTest();

            // CanSleepInTest();

            // SumDoubleTest();

            // Diff21Test();

            // ParrotTroubleTest();

            // Makes10Test();

            // NearHundredTest();

            // PosNegTest();

            // NotStringTest();

            // MissingCharTest();

            // FrontBackTest();

            // Front3Test();

            // BackAroundTest();

            // Multiple3or5Test();

            // StartHiTest();

            // IcyHotTest();

            // Between10and20Test();

            // HasTeenTest();

            // SoAloneTast();

            // RemoveDelTets();

            // IxStartTest();

            // StartOzTest();

            // MaxTest();

            // CloserTets();

            // GotETest();

            // EndUpTest();

            // EveryNthTest();                   


        }

        private static void EveryNthTest()
        {
            EveryNth everyNth = new EveryNth();
            everyNth.Run();
        }

        private static void EndUpTest()
        {
            EndUp endUp = new EndUp();
            endUp.Run();
        }

        private static void GotETest()
        {
            GotE got = new GotE();
            got.Run();
        }

        private static void CloserTets()
        {
            Closer closer = new Closer();
            closer.Run();
        }

        private static void MaxTest()
        {
            Max max = new Max();
            max.Run();
        }

        private static void StartOzTest()
        {
            StartOz startOz = new StartOz();
            startOz.Run();
        }

        private static void IxStartTest()
        {
            IxStart ixStart = new IxStart();
            ixStart.Run();
        }

        private static void RemoveDelTets()
        {
            RemoveDel removeDel = new RemoveDel();
            removeDel.Run(); 
        }

        private static void SoAloneTast()
        {
            SoAlone soAlone = new SoAlone();
            soAlone.Run();
        }

        private static void HasTeenTest()
        {
            HasTeen hasTeen = new HasTeen();
            hasTeen.Run();
        }

        private static void Between10and20Test()
        {
            Between10and20 between = new Between10and20();
            between.Run();
        }

        private static void IcyHotTest()
        {
            IcyHot icyHot = new IcyHot();
            icyHot.Run();
        }

        private static void StartHiTest()
        {
            StartHi hi = new StartHi();
            hi.Run();
        }

        private static void Multiple3or5Test()
        {
            Multiple3or5 multiple = new Multiple3or5();
            multiple.Run();
        }

        private static void BackAroundTest()
        {
            var backAround = new BackAround();
            backAround.Run();
        }

        private static void Front3Test()
        {
            var front3 = new Front3();
            front3.Run();
        }

        private static void FrontBackTest()
        {
            var frontBack = new FrontBack();
            frontBack.Run();
        }

        private static void MissingCharTest()
        {
            var missingChar = new MissingChar();
            missingChar.Run();
        }

        private static void NotStringTest()
        {
            var notString = new NotString();
            notString.Run();
        }

        private static void PosNegTest()
        {
            var num = new PosNeg();
            num.Run();
        }

        private static void NearHundredTest()
        {
            var num = new NearHundred();
            num.Run();
        }

        private static void Makes10Test()
        {
            var makes = new Makes10();
            makes.Run();
        }

        private static void ParrotTroubleTest()
        {
            var trouble = new ParrotTrouble();
            trouble.Run();
        }

        private static void Diff21Test()
        {
            var diff = new Diff21();
            diff.Run();
        }

        private static void SumDoubleTest()
        {
            var sum = new SumDouble();
            sum.Run();
        }

        private static void CanSleepInTest()
        {
            var canSleepIn = new CanSleepIn();
            canSleepIn.CanSleep();
        }

        private static void InTroubleTest()
        {
            var inTrouble = new InTrouble();
            inTrouble.Play();
        }

        private static void ArrayTest()
        {
            //FirstLast6Test();

            //SameFirstLastTest();

            //MakePiTest();

            //CommonEndTest();

            //Sum();

            //RotateLeftTest();

            //ReversTest();

            //HigherWinsTest();

            //GetMiddleTest();

            //HasEvenTest();

            //KeepLastTest();

            //Double23Test();

            //Fix23Test();

            //Unlucky1Tast();

            // Make2Test();
        }

        private static void Make2Test()
        {
            var firstArray = new Make2();
            firstArray.PrintArray();

            var secondArray = new Make2();
            secondArray.PrintArray();

            var newArray = firstArray.MakeTwo(firstArray, secondArray);
            Console.Write("New array is: ");

            foreach (var elm in newArray)
            {
                Console.Write(elm + " ");
            }
        }

        private static void Unlucky1Tast()
        {
            var array = new Unlucky1();
            Console.WriteLine(array.CheckArray());
        }

        private static void Fix23Test()
        {
            var array = new Fix23();

            array.PrintArray();

            Console.WriteLine("\nPress key to change array...");
            Console.ReadKey();

            array.ChangeArray();
        }

        private static void Double23Test()
        {
            var array = new Double23();

            array.PrintArray();

            Console.WriteLine(array.CheckArray());
        }

        private static void KeepLastTest()
        {
            var keepLast = new KeepLast();

            keepLast.PrintArray();
            var newArray = keepLast.KeepLastElement();

            Console.Write("New array is: ");
            foreach (var elem in newArray)
            {
                Console.Write(elem + " ");
            }
        }

        private static void HasEvenTest()
        {
            var array = new HasEven();
            array.CreateArray();
            array.PrintArray();

            Console.WriteLine(array.IsHasEven());
        }

        private static void GetMiddleTest()
        {
            var firstArray = new GetMiddle();
            firstArray.CreateArray();
            firstArray.PrintArray();

            var secondArray = new GetMiddle();
            secondArray.CreateArray();
            secondArray.PrintArray();

            Console.WriteLine("\nPress key to get the middele array...");
            Console.ReadKey();
            Console.Clear();


            var newArray = firstArray.GetMiddelArrays(firstArray, secondArray);

            foreach (var item in newArray)
            {
                Console.Write(item + " ");
            }
        }

        private static void HigherWinsTest()
        {
            var higherWins = new HigherWins();
            higherWins.CreateArray();
            higherWins.PrintArray();

            Console.WriteLine("\nPress key to change array...");
            Console.ReadKey();

            higherWins.ChangeArray();
            higherWins.PrintArray();
        }

        private static void ReversTest()
        {
            var reverse = new Reverse();
            reverse.CreateArray();
            reverse.PrintArray();

            Console.WriteLine("\nPress key to revers array...");
            Console.ReadKey();

            reverse.ReverseArray();
            reverse.PrintArray();
        }

        private static void FirstLast6Test()
        {
            var firstLast6 = new FirstLast6();
            Console.WriteLine("\n" + firstLast6.CheckArray());
        }

        private static void SameFirstLastTest()
        {
            var sameFirstLast = new SameFirstLast();
            Console.WriteLine("\n" + sameFirstLast.CheckArray());
        }

        private static void MakePiTest()
        {
            var makePi = new MakePi();
            makePi.CreateArrayPi();
            makePi.PrintArray();
        }

        private static void CommonEndTest()
        {
            var commonEnd = new CommonEnd();
            var firstArray = new MyArray();

            firstArray.CreateArray();
            firstArray.PrintArray();

            Console.ReadKey();

            var secondArray = new MyArray();

            secondArray.CreateArray();
            secondArray.PrintArray();

            Console.ReadKey();
            Console.Clear();

            Console.WriteLine(commonEnd.CheckArray(firstArray, secondArray));
        }

        private static void Sum()
        {
            var array = new MyArray();
            array.CreateArray();

            var sum = new Sum();
            sum.SumElements(array);
        }

        private static void RotateLeftTest()
        {
            var rotateLeft = new RotateLeft();

            rotateLeft.CreateArray();
            rotateLeft.PrintArray();

            Console.Clear();
            Console.WriteLine("Press key to rotate array...");

            Console.ReadKey();
            Console.Clear();

            rotateLeft.RotateLeftArray();

            rotateLeft.PrintArray();
        }
    }
}