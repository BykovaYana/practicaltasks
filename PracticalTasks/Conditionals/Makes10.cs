﻿using System;

namespace PracticalTasks.Conditionals
{
    internal class Makes10
    {
        private int firstNumber;
        private int secondNumber;

        public Makes10()
        {
            firstNumber = 0;
            secondNumber = 0;
        }

        private void SetNumbers()
        {
            Console.Write("Please, enter the  first number: ");
            firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please, enter the  second number: ");
            secondNumber = Convert.ToInt32(Console.ReadLine());
        }

        private bool CheckNumbers()
        {
            if (firstNumber == 10 || secondNumber == 10 ||
                firstNumber + secondNumber == 10)
            {
                return true;
            }
            return false;
        }

        public void Run()
        {
            SetNumbers();
            Console.WriteLine(CheckNumbers());
        }
    }
}