﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Conditionals
{
    class SoAlone

    {
        private int firstNumber;
        private int secondNumber;
        private bool IsSoAlone()
        {
            if ((firstNumber >= 13 && firstNumber <= 19)
                && (secondNumber >= 13 && secondNumber <= 19))
            {
                Console.WriteLine("Both numbers are teen!");
                return false;
            }
            else if (firstNumber >= 13 && firstNumber <= 19)
            {
                Console.WriteLine("First number is teen!");
                return true;
            }
            else if (secondNumber >= 13 && secondNumber <= 19)
            {
                Console.WriteLine("Second number is teen!");
                return true;
            }
            return false;

        }

        private void SetNumbers()
        {
            Console.Write("Please, enter the first number: ");
            firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please, enter the second number: ");
            secondNumber = Convert.ToInt32(Console.ReadLine());
        }

        public void Run()
        {
            SetNumbers();
            Console.WriteLine(IsSoAlone());
        }
    }
}
