﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Conditionals
{
    class RemoveDel
    {
        private string originalString;
        private string newString;

        private void SetString()
        {
            Console.Write("Please, enter the string: ");
            originalString = Console.ReadLine();
        }

        private bool CheckTheString()
        {
            if (originalString.Contains("del"))
            {
                if (originalString.IndexOf("e") != 1)
                {
                    return true;
                }
                else
                {
                    Console.WriteLine("The string 'del' appears starting at index 0!");
                    return false;
                }           
            }
            Console.WriteLine("The string does not contain 'del'!");
            return false;
        }

        private void ChangeTheString()
        {
            if (CheckTheString())
            {
                newString = originalString.Replace("del", "");
            }
            else
            {
                newString = originalString;
            }
        }

        public void Run()
        {
            SetString();
            ChangeTheString();
            Console.WriteLine("Original string is: {0}", originalString);
            Console.WriteLine("New string is: {0}", newString);
        }
    }
}
