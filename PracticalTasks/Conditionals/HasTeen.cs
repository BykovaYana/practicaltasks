﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Conditionals
{
    class HasTeen
    {
        private int firstNumber;
        private int secondNumber;
        private int thirdNumber;

        public HasTeen()
        {
            firstNumber = 0;
            secondNumber = 0;
            thirdNumber = 0;
        }

        private void SetNumbers()
        {
            Console.Write("Please, enter the first number: ");
            firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please, enter the second number: ");
            secondNumber = Convert.ToInt32(Console.ReadLine());


            Console.Write("Please, enter the third number: ");
            thirdNumber = Convert.ToInt32(Console.ReadLine());
        }

        private bool IsHasTeen()
        {
            if (firstNumber >= 13 && firstNumber <= 19)
            {
                Console.WriteLine("First number is teen!");
                return true;
            }
            else if (secondNumber >= 13 && secondNumber <= 19)
            {
                Console.WriteLine("Second number is teen!");
                return true;
            }
            else if (thirdNumber >= 13 && thirdNumber <= 19)
            {
                Console.WriteLine("Third number is teen!");
                return true;
            }
            return false;
        }

        public void Run()
        {
            SetNumbers();
            Console.WriteLine(IsHasTeen());

        }
    }
}
