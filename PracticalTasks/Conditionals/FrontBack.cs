﻿using System;
using System.Linq;

namespace PracticalTasks.Conditionals
{
    internal class FrontBack
    {
        private string newString;
        private string originalString;

        public FrontBack()
        {
            newString = "";
            originalString = "";
        }

        private void SetString()
        {
            Console.Write("Please, enter the string: ");
            originalString = Console.ReadLine();
        }

        private void ChangeTheString()
        {
            var bufString = originalString.ToList();
            if (bufString.Count - 1 > 1)
            {
                var buf = bufString[0];
                bufString[0] = bufString[bufString.Count - 1];
                bufString[bufString.Count - 1] = buf;
                newString = string.Join("", bufString.ToArray());
            }
            else
            {
                newString = originalString;
            }
        }

        public void Run()
        {
            SetString();
            ChangeTheString();

            Console.Clear();
            Console.WriteLine("Old string is: " + originalString);
            Console.WriteLine("New string is: " + newString);
        }
    }
}