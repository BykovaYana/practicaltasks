﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Conditionals
{
    class IxStart
    {
        private string originalString;

        private void SetString()
        {
            Console.Write("Please, enter the string: ");
            originalString = Console.ReadLine();
        }

        private bool CheckTheString()
        {
            if (originalString.Contains("ix"))
            {
                if (originalString.IndexOf("i") == 1)
                {
                    return true;
                }
            }
                Console.WriteLine("The string does not start begins with '*ix'!");
                return false;            
        }
        public void Run()
        {
            SetString();
            Console.WriteLine(CheckTheString());

        }
    }
}
