﻿using System;
using System.Text;

namespace PracticalTasks.Conditionals
{
    internal class BackAround
    {
        private string newString;
        private string originalString;

        public BackAround()
        {
            originalString = "";
            newString = "";
        }

        private void SetString()
        {
            var isValid = false;
            while (!isValid)
            {
                Console.Write("Please, enter the string: ");
                originalString = Console.ReadLine();
                if (originalString.Length < 1)
                {
                    Console.Clear();
                    Console.WriteLine("The string length must be greater than 1! Try again!");
                }
                else
                {
                    isValid = true;
                }
            }
        }

        private void ChangeString()
        {
            var bufString = new StringBuilder(originalString);
            newString = originalString.Insert(0, bufString[bufString.Length - 1].ToString());
            newString += bufString[bufString.Length - 1];
        }

        public void Run()
        {
            SetString();
            ChangeString();

            Console.Clear();
            Console.WriteLine("Old string is: " + originalString);
            Console.WriteLine("New string is: " + newString);
        }
    }
}