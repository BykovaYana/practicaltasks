﻿using System;
using System.Text;

namespace PracticalTasks.Conditionals
{
    internal class Front3
    {
        private string newString;
        private string originalString;

        public Front3()
        {
            originalString = "";
            newString = "";
        }

        private void SetString()
        {
            Console.Write("Please, enter the string: ");
            originalString = Console.ReadLine();
        }

        private void ChangeString()
        {
            var bufString = new StringBuilder(originalString);
            if (originalString.Length == 0)
            {
                Console.WriteLine("Empty string!");
                return;
            }
            if (originalString.Length > 3)
            {
                for (var j = 0; j < 3; j++)
                {
                    for (var i = 0; i < 3; i++)
                    {
                        newString += bufString[i].ToString();
                    }
                }
            }
            else
            {
                for (int i = 0; i < 3; i++)
                {
                    newString += originalString;
                }

            }
        }

        public void Run()
        {
            SetString();
            ChangeString();

            Console.Clear();
            Console.WriteLine("Old string is: " + originalString);
            Console.WriteLine("New string is: " + newString);
        }
    }
}