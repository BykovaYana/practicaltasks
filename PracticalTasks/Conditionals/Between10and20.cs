﻿using System;

namespace PracticalTasks.Conditionals
{
    internal class Between10and20
    {
        private int firstNumber;
        private int secondNumber;

        public Between10and20()
        {
            firstNumber = 0;
            secondNumber = 0;
        }

        private void SetNumbers()
        {
            Console.Write("Please, enter the first number: ");
            firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please, enter the second number: ");
            secondNumber = Convert.ToInt32(Console.ReadLine());
        }

        private bool CheckNumbers()
        {
            if (firstNumber >= 10 && firstNumber <= 20
                &&
                secondNumber >= 10 && secondNumber <= 20)
            {
                Console.WriteLine("Both numbers are in the range 10..20! ");
                return true;
            }

            else if (firstNumber >= 10 && firstNumber <= 20)
            {
                Console.WriteLine("First number is in the range 10..20!");
                return true;
            }

            else if (secondNumber >= 10 && secondNumber <= 20)
            {
                Console.WriteLine("Second number is in the range 10..20!");
                return true;
            }
            
            return false;
        }

        public void Run()
        {
            SetNumbers();
            Console.WriteLine(CheckNumbers());
        }
    }
}