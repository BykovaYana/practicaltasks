﻿using System;

namespace PracticalTasks.Conditionals
{
    internal class NearHundred
    {
        private readonly int hundred;
        private readonly int twoHundred;
        private int number;

        public NearHundred()
        {
            hundred = 100;
            twoHundred = 200;
            number = 0;
        }

        private void SetNumber()
        {
            Console.Write("Please, enter the number: ");
            number = Convert.ToInt32(Console.ReadLine());
        }

        private bool CheckTheNumber()
        {
            if (Math.Abs(hundred - number) <= 10)
            {
                Console.WriteLine("The number near 100");
                return true;
            }
            if (Math.Abs(twoHundred - number) <= 10)
            {
                Console.WriteLine("The number near 200");
                return true;
            }
            return false;
        }

        public void Run()
        {
            SetNumber();
            Console.WriteLine(CheckTheNumber());
        }
    }
}