﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Conditionals
{
    class ParrotTrouble
    {
        private bool isTalking;
        private int hour;

        public ParrotTrouble()
        {
            hour = 0;
            isTalking = false;
        }

        private bool IsTrouble()
        {
            if (hour >= 20 || hour < 7)
            {
                if (isTalking == true)
                {
                    return true;
                }
            }
            return false;
        }

        private bool ConvertStringToBool(string choice)
        {
            if (choice.Equals("Y", StringComparison.CurrentCultureIgnoreCase))
            {
                return true;
            }
            return false;
        }

        private void SetParameters()
        {
            var isValidChoice = false;
            string isTalking;
            while (!isValidChoice)
            {
                Console.Write("How many hours now?\t");
                hour = Convert.ToInt32(Console.ReadLine());
                if (hour > 23)
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input. Try again.");
                }
                else
                {
                    isValidChoice = true;
                }
            }
            isValidChoice = false;

            while (!isValidChoice)
            {
                Console.WriteLine("Is parrot says?");
                Console.Write("Yes - press Y, No - press N.\t");
                isTalking = Console.ReadLine();

                if (isTalking.Equals("Y", StringComparison.CurrentCultureIgnoreCase) ||
                    isTalking.Equals("N", StringComparison.CurrentCultureIgnoreCase))
                {
                    this.isTalking = ConvertStringToBool(isTalking);
                    isValidChoice = true;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input. Try again.");
                }
            }

        }

        public void Run()
        {
            SetParameters();
            if (IsTrouble())
            {
               Console.WriteLine("We have problems!"); 
            }
            else
            {
                Console.WriteLine("Everything is fine");
            }

        }
    }
}
