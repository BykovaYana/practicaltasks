﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Conditionals
{
    class EveryNth
    {
        private string newString;
        private string originalString;
        private int length;
        private int index;

        public EveryNth()
        {
            length = 0;
            index = 0;
        }

        private void SetString()
        {
            while (length == 0)
            {
                //Console.Clear();
                Console.Write("Please, enter the string: ");
                originalString = Console.ReadLine();
                length = originalString.Length;
                if (length == 0)
                {
                    Console.WriteLine("The length of string is 0! Try again!");
                }
            }

            while (index == 0 || index > originalString.Length - 1)
            {
                Console.Write("Please, enter the index: ");
                index = Convert.ToInt32(Console.ReadLine());
                if (index == 0)
                {
                    Console.WriteLine("Index must be greater than 0! Try again!");
                }
                if (index > originalString.Length - 1)
                {
                    Console.WriteLine("The index is more than a line! Try again!");
                }
            }
        }

        private void ChangeString()
        {
            var bufString = originalString.ToCharArray();
            for (int i = 0; i <= length/2; i++)
            {
                newString += bufString[i*index].ToString();              
            }
        }
        public void Run()
        {
            SetString();
            ChangeString();
            Console.WriteLine(originalString);
            Console.WriteLine(newString);
        }

    }
}
