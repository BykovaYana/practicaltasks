﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Conditionals
{
    class Max
    {
        private int firstNumber;
        private int secondNumber;
        private int thirdNumber;
        private int max;

        private void SetNumbers()
        {
            Console.Write("Please, enter the first number: ");
            firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please, enter the second number: ");
            secondNumber = Convert.ToInt32(Console.ReadLine());


            Console.Write("Please, enter the third number: ");
            thirdNumber = Convert.ToInt32(Console.ReadLine());
        }

        private void GetTheMax()
        {
            if (firstNumber >= secondNumber)
            {
                max = firstNumber;
            }
            else
            {
                max = secondNumber;
            }
            if (secondNumber >= thirdNumber)
            {
                max = secondNumber;
            }
            else
            {
                max = thirdNumber;
            }
            if (firstNumber >= thirdNumber)
            {
                max = firstNumber;
            }
            else
            {
                max = thirdNumber;
            }

        }

        public void Run()
        {
            SetNumbers();
            GetTheMax();
            Console.Write("Max number is: {0}", max);
        }
    }
}
