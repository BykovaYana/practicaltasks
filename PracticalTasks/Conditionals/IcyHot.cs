﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Conditionals
{
    class IcyHot
    {
        private int firstTemp;
        private int secondTemp;

        public IcyHot()
        {
            firstTemp = 0;
            secondTemp = 0;
        }

        private void SetTemps()
        {
            Console.Write("Please, enter firs temperature: ");
            firstTemp = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please, enter second temperature: ");
            secondTemp = Convert.ToInt32(Console.ReadLine());
        }

        private bool CheckTemps()
        {
            if (firstTemp < 0 && secondTemp > 100)
            {
                return true;
            }
            else if(secondTemp <0 && firstTemp >100)
            {
                return true;
            }
            return false;
        }

        public void Run()
        {
            SetTemps();
            Console.WriteLine(CheckTemps());
        }
    }
}
