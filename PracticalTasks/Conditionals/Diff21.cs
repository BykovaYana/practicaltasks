﻿using System;

namespace PracticalTasks.Conditionals
{
    internal class Diff21
    {
        private readonly int const21;
        private int difference;
        private int number;

        public Diff21()
        {
            const21 = 21;
            number = 0;
            difference = 0;
        }

        private void SetNumber()
        {
            Console.Write("Please, enter the number: ");
            number = Convert.ToInt32(Console.ReadLine());
        }

        private void Difference()
        {
            if (number > const21)
            {
                difference = Math.Abs(const21 - number)*2;
                Console.WriteLine("The selected number is greater than 21. The double absolute difference is: {0}",
                    difference);
            }
            else
            {
                difference = Math.Abs(const21 - number);
                Console.WriteLine("The absolute difference is: {0}", difference);
            }
        }

        public void Run()
        {
            SetNumber();
            Difference();
        }
    }
}