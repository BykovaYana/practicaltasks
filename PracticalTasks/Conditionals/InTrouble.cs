﻿using System;

namespace PracticalTasks.Conditionals
{
    internal class InTrouble
    {
        private bool firstSmile;
        private bool secondSmile;

        private string nameFirstChild;
        private string nameSecondChild;

        private Random rng;

        public InTrouble()
        {
            rng = new Random();
            firstSmile = false;
            secondSmile = false;
        }

        private void SetName()
        {
            Console.Write("Please, enter the name of the first child: ");
            nameFirstChild = Console.ReadLine();

            Console.Write("Please, enter the name of the second child: ");
            nameSecondChild = Console.ReadLine();
        }

        private string SmileOrNot()
        {
            var automaticChoice = rng.Next(0, 2);
            if (automaticChoice != 1)
            {
                return "N";
            }
            return "Y";
        }

        private bool ConvertStringToBool(string choice)
        {
            if (choice.Equals("Y", StringComparison.CurrentCultureIgnoreCase))
            {
                return true;
            }
            return false;
        }

        private void WhoIsSmilingManually()
        {
            var isValidChoice = false;
            string firstSmile;
            string secondSmile;
            while (!isValidChoice)
            {
                Console.WriteLine("Is {0} smiles?", nameFirstChild);
                Console.Write("Yes - press Y, No - press N.\t");
                firstSmile = Console.ReadLine();

                if (firstSmile.Equals("Y", StringComparison.CurrentCultureIgnoreCase) ||
                    firstSmile.Equals("N", StringComparison.CurrentCultureIgnoreCase))
                {
                    this.firstSmile = ConvertStringToBool(firstSmile);
                    isValidChoice = true;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input. Try again.");
                }
            }
            isValidChoice = false;
            while (!isValidChoice)
            {
                Console.WriteLine("Is {0} smiles?", nameSecondChild);
                Console.Write("Yes - press Y, No - press N.\t");
                secondSmile = Console.ReadLine();

                if (secondSmile.Equals("Y", StringComparison.CurrentCultureIgnoreCase) ||
                    secondSmile.Equals("N", StringComparison.CurrentCultureIgnoreCase))
                {
                    this.secondSmile = ConvertStringToBool(secondSmile);
                    isValidChoice = true;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input. Try again.");
                }
            }
        }

        private void WhoIsSmilingAutomatic()
        {
            firstSmile = ConvertStringToBool(SmileOrNot());
            secondSmile = ConvertStringToBool(SmileOrNot());
        }

        private string ConvertSmile(bool smile)
        {
            if (smile)
            {
                return "smile";
            }
            return "not smile";
        }

        private bool AreWeInTrouble()
        {
            if (firstSmile == secondSmile)
            {
                return true;
            }
            return false;
        }

        public void Play()
        {
            var isValidChoice = false;
            SetName();
            while (!isValidChoice)
            {
                Console.Write(
                    "If you want to manually decide smiling kids or not, press 1, to realize automatic choice, press 2. ");

                var key = Convert.ToInt32(Console.ReadLine());
                switch (key)
                {
                    case 1:
                    {
                        Console.Clear();
                        WhoIsSmilingManually();
                        Console.WriteLine(nameFirstChild + " is " + ConvertSmile(firstSmile) + ". " + nameSecondChild +
                                          " is " + ConvertSmile(secondSmile));
                        isValidChoice = true;
                        break;
                    }
                    case 2:
                    {
                        Console.Clear();
                        WhoIsSmilingAutomatic();
                        Console.WriteLine(nameFirstChild + " is " + ConvertSmile(firstSmile) + ". " + nameSecondChild +
                                          " is " + ConvertSmile(secondSmile));
                        isValidChoice = true;
                        break;
                    }
                    default:
                    {
                        isValidChoice = false;
                        Console.Clear();
                        Console.WriteLine("Not valid choice...");
                        break;
                    }
                }
            }

            if (AreWeInTrouble())
            {
                Console.WriteLine("We are in trouble. {0} and {1} are {2}", nameFirstChild, nameSecondChild,
                    AreWeInTrouble());
            }
            else
            {
                Console.WriteLine("We are not in trouble.");
            }
        }
    }
}