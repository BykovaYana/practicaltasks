﻿using System;

namespace PracticalTasks.Conditionals
{
    internal class StartHi
    {
        private string[] bufString;
        private string originalString;

        public StartHi()
        {
            originalString = "";
            bufString = new[] {""};
        }

        private bool CheckTheString()
        {
            bufString = originalString.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            if (bufString[0].Equals("hi", StringComparison.CurrentCultureIgnoreCase))
            {
                return true;
            }
            return false;
        }

        private void SetString()
        {
            Console.Write("Please, enter the string: ");
            originalString = Console.ReadLine();
        }

        public void Run()
        {
            SetString();
            Console.WriteLine(CheckTheString());
        }
    }
}