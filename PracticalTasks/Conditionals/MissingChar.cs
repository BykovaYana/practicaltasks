﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PracticalTasks.Conditionals
{
    internal class MissingChar
    {
        private int index;
        private string newString;
        private string originalString;

        public MissingChar()
        {
            index = 0;
            originalString = "";
            newString = "";
        }

        private void SetStringAndIndex()
        {
            Console.Write("Please, enter the string: ");
            originalString = Console.ReadLine();
            var isValid = false;

            while (!isValid)
            {
                Console.Write("Please, enter the index: ");
                var buf = Convert.ToInt32(Console.ReadLine());
                if (buf >= 0 && buf < originalString.Length)
                {
                    index = buf;
                    isValid = true;
                    Console.Clear();
                }
                else
                {
                    Console.WriteLine("Is invalid index");
                }
            }
        }

        private void ChangeString()
        {
            var bufString = originalString.ToList();
            bufString.RemoveAt(index);
            newString = string.Join("", bufString.ToArray());
        }

        public void Run()
        {
            SetStringAndIndex();
            ChangeString();

            Console.Clear();
            Console.WriteLine("Old string is: " + originalString);
            Console.WriteLine("New string is: " + newString);
        }
    }
}