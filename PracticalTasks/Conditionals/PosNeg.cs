﻿using System;

namespace PracticalTasks.Conditionals
{
    internal class PosNeg
    {
        private int firstNumber;
        private bool negative;
        private Random rng;
        private int secondNumber;

        public PosNeg()
        {
            negative = false;
            firstNumber = 0;
            secondNumber = 0;
        }

        private void SetNumbers()
        {
            Console.Write("Please, enter the first number: ");
            firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please, enter the second number: ");
            secondNumber = Convert.ToInt32(Console.ReadLine());
        }

        private void IsNagative()
        {
            var automaticChoice = rng.Next(0, 2);
            if (automaticChoice != 1)
            {
                negative = false;
            }
            negative = true;
        }

        private bool CheckNumbers()
        {
            if (negative && firstNumber < 0 && secondNumber < 0)
            {
                Console.WriteLine("Both number are negative.");
                return true;
            }
            else if (negative == false)
            {
                if (firstNumber < 0 && secondNumber > 0)
                {
                    Console.WriteLine("First number is negative.");
                    return true;
                }
                else if (firstNumber > 0 && secondNumber < 0)
                {
                    Console.WriteLine("Second number is negative.");
                    return true;
                }
            }
            return false;
        }

        public void Run()
        {
            SetNumbers();

            Console.Clear();
            Console.Write("Additional parameter is: " + negative + "\n");

            Console.WriteLine(CheckNumbers());

        }
    }
}