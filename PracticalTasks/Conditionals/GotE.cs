﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Conditionals
{
    class GotE
    {
        private string originalString;
        private int count;

        public GotE()
        {
            count = 0;
            originalString = "";
        }

        private void SetString()
        {
            Console.Write("Please, enter the string: ");
            originalString = Console.ReadLine();
        }

        private bool CheckTheString()
        {
            var bufString = originalString.ToCharArray();
            foreach (var elm in bufString)
            {
                if (elm == 'e')
                {
                    count++;
                }
            }
            if (count == 1 || count == 3)
            {
                return true;               
            }
            return false;
        }

        public void Run()
        {
            SetString();
            Console.WriteLine(CheckTheString());
        }
    }
}
