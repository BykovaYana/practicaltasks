﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Conditionals
{
    class NotString
    {
        private string originalString;
        private string[] bufString;
        private string newString;
        
        public NotString()
        {
            bufString = new[] {""};
            originalString = "";
            newString = "";
        }

        private void SetString()
        {
            Console.Write("Please, enter the string: ");
            originalString = Console.ReadLine();
        }

        private bool CheckTheString()
        {
            bufString = originalString.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            if (bufString[0].Equals("not", StringComparison.CurrentCultureIgnoreCase))
            {
                return true;
            }
            return false;

        }

        private void ChangeString()
        {
            if (!CheckTheString())
            {
                newString = originalString.Insert(0, "not ");
                Console.Write("New string is: " + newString);
            }
            else
            {
                Console.WriteLine("Line already has NOT.");
                Console.Write("Line is: " + originalString);
            }
        }

        public void Run()
        {
            SetString();
            Console.Clear();
            ChangeString();
        }
    }
}
