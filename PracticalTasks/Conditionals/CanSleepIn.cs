﻿using System;

namespace PracticalTasks.Conditionals
{
    internal class CanSleepIn
    {
        private bool isVacation;
        private bool isWeekday;

        public CanSleepIn()
        {
            isVacation = false;
            isWeekday = false;
        }

        private bool ConvertStringToBool(string choice)
        {
            if (choice.Equals("Y", StringComparison.CurrentCultureIgnoreCase))
            {
                return true;
            }
            return false;
        }

        private void WhatTheDay()
        {
            var isValidChoice = false;
            string isWeekday;
            string isVacation;
            while (!isValidChoice)
            {
                Console.WriteLine("Is a weekday today?");
                Console.Write("Yes - press Y, No - press N.\t");
                isWeekday = Console.ReadLine();

                if (isWeekday.Equals("Y", StringComparison.CurrentCultureIgnoreCase) ||
                    isWeekday.Equals("N", StringComparison.CurrentCultureIgnoreCase))
                {
                    this.isWeekday = ConvertStringToBool(isWeekday);
                    isValidChoice = true;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input. Try again.");
                }
            }

            isValidChoice = false;

            while (!isValidChoice)
            {
                Console.WriteLine("You're on vacation?");
                Console.Write("Yes - press Y, No - press N.\t");
                isVacation = Console.ReadLine();

                if (isVacation.Equals("Y", StringComparison.CurrentCultureIgnoreCase) ||
                    isVacation.Equals("N", StringComparison.CurrentCultureIgnoreCase))
                {
                    this.isVacation = ConvertStringToBool(isVacation);
                    isValidChoice = true;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input. Try again.");
                }
            }
        }

        private bool CheckTheDay()
        {
            if (isVacation || isWeekday == false)
            {
                return true;
            }
            return false;
        }

        public void CanSleep()
        {
            WhatTheDay();
            if (CheckTheDay())
            {
                Console.WriteLine("You can sleep in!");
            }
            else
            {
                Console.WriteLine("You can not sleep in!");
            }
        }
    }
}