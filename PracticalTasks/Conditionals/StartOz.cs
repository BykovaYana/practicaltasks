﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Conditionals
{
    class StartOz
    {
        private string originalString;
        private string newString;

        private void SetString()
        {
            Console.Write("Please, enter the string: ");
            originalString = Console.ReadLine();
        }

        private void CreateNewLine()
        {
            if (originalString.Contains("oz"))
            {
                if (originalString.IndexOf("o") == 0)
                {
                    newString = "oz";
                }
            }

            else if (originalString.Contains("o"))
            {
                if (originalString.IndexOf("o") == 0)
                {
                    newString = "o";
                }
            }

            else if (originalString.Contains("z"))
            {
                if (originalString.IndexOf("z") == 1)
                {
                    newString = "z";
                }
            }

            else
            {
                Console.WriteLine("Line does not satisfy conditions!");
                newString = null;
            }
        }
        public void Run()
        {
            SetString();
            CreateNewLine();
            Console.WriteLine("Original string is: {0}", originalString);
            Console.WriteLine("New string is: {0}", newString);
        }
    }
}
