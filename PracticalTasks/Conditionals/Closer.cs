﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Conditionals
{
    class Closer
    {
        private int firstNumber;
        private int secondNumber;


        public Closer()
        {
            firstNumber = 0;
            secondNumber = 0;
        }

        private void SetNumbers()
        {
            Console.Write("Please, enter the first number: ");
            firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please, enter the second number: ");
            secondNumber = Convert.ToInt32(Console.ReadLine());

        }

        private void CheckNumbers()
        {
            if (10 - firstNumber == 10 - secondNumber)
            {
                Console.WriteLine('0');
            }
            else if (10 - firstNumber < 10 - secondNumber)
            {
                Console.WriteLine(firstNumber);
            }
            else if (10 - firstNumber > 10 - secondNumber)
            {
                Console.WriteLine(secondNumber);
            }
        }

        public void Run()
        {
            SetNumbers();
            CheckNumbers();
        }
    }
}
