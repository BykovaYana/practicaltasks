﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Conditionals
{
    class SumDouble
    {
        private int firstNumber;
        private int secondNumber;
        private int sum;

        public SumDouble()
        {
            firstNumber = 0;
            secondNumber = 0;
            sum = 0;
        }
        
        private void SetNumbers()
        {
            Console.Write("Please, enter the first number: ");
            firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please, enter the second number: ");
            secondNumber = Convert.ToInt32(Console.ReadLine());
        }

        private void Sum()
        {
            if (secondNumber == firstNumber)
            {
                sum = (firstNumber + secondNumber)*2;
                Console.WriteLine("Numbers are the equal, the double sum their is: " + sum);
            }
            else
            {
                sum = firstNumber + secondNumber;
                Console.WriteLine("Sum of numbers is: " + sum);
            }
        }

        public void Run()
        {
            SetNumbers();
            Sum();
        }
    }
}
