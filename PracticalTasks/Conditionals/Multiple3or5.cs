﻿using System;

namespace PracticalTasks.Conditionals
{
    internal class Multiple3or5
    {
        private int number;

        public Multiple3or5()
        {
            number = 0;
        }

        private void SetNumber()
        {
            Console.Write("Please, enter the number: ");
            number = Convert.ToInt32(Console.ReadLine());
        }

        private bool CheckNumber()
        {
            if (number > 0)
            {
                if (number%3 == 0 || number%5 == 0)
                {
                    return true;
                }
                return false;
            }
            Console.WriteLine("Number is negative!");
            return false;
        }

        public void Run()
        {
            SetNumber();
            Console.WriteLine(CheckNumber());
        }
    }
}