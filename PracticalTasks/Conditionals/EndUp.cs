﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Conditionals
{
    class EndUp
    {
        private string newString;
        private string originalString;
        private int length;
        private const int endUp = 3;

               private void SetString()
        {
            Console.Write("Please, enter the string: ");
            originalString = Console.ReadLine();
            length = originalString.Length;

        }

        private void ChangeTheString()
        {
            if (length <= 3)
            {
                newString = originalString.ToUpper();
            }
            else
            {
                var up = length - 3;
                newString = originalString.Substring(0, up) + originalString.Substring(up, endUp).ToUpper();
            }
        }
        public void Run()
        {
            SetString();
            ChangeTheString();

            Console.Write("Original string is: {0}", originalString);
            Console.Write("\nNew string is: {0}", newString);
        }
    }
}
