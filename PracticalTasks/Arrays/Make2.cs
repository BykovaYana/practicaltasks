﻿namespace PracticalTasks.Arrays
{
    internal class Make2 : MyArray
    {
        private readonly int lenghtNewArray;
        private readonly int[] newArray;

        public Make2()
        {
            lenghtNewArray = 2;
            newArray = new[] {0, 0};
            CreateArray();
        }

        public int[] MakeTwo(Make2 firstArray, Make2 secondArray)
        {
            if (firstArray._lenght != 0)
            {
                if (firstArray._lenght == 1)
                {
                    newArray[0] = firstArray._array[0];
                    newArray[1] = secondArray._array[0];
                }
                else
                {
                    for (var i = 0; i < lenghtNewArray; i++)
                    {
                        newArray[i] = firstArray._array[i];
                    }
                }
            }
            else
            {
                for (var i = 0; i < lenghtNewArray; i++)
                {
                    newArray[i] = secondArray._array[i];
                }
            }


            return newArray;
        }
    }
}