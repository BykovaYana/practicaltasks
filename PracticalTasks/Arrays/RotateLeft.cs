﻿using System;

namespace PracticalTasks.Arrays
{
    internal class RotateLeft : MyArray
    {
        private readonly int[] bufArray;
        private int _bias;

        public RotateLeft()
        {
            //SetTheLenghtArray();
            bufArray = new int[_lenght];
            _bias = 1;
        }

        private void GetBias()
        {
            Console.Write("Enter the bais: ");
            _bias = Convert.ToInt32(Console.ReadLine());
        }

        public void RotateLeftArray()
        {
            GetBias();

            for (var i = 0; i < _lenght; i++)
            {
                if (i >= _bias)
                {
                    bufArray[i] = _array[i - _bias];
                }
                else
                {
                    bufArray[i] = _array[i - _bias + _lenght];
                }
            }

            for (var i = 0; i < _lenght; i++)
            {
                _array[i] = bufArray[i];
            }
        }
    }
}