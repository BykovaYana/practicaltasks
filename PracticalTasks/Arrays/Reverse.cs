﻿namespace PracticalTasks.Arrays
{
    internal class Reverse : MyArray
    {
        private int buf;

        public Reverse()
        {
            //  SetTheLenghtArray();
            buf = 0;
        }

        public void ReverseArray()
        {
            for (int i = 0, j = _lenght - 1; i < _lenght/2; i++, j--)
            {
                buf = _array[i];
                _array[i] = _array[j];
                _array[j] = buf;
            }
        }
    }
}