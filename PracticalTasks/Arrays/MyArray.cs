﻿using System;

namespace PracticalTasks.Arrays
{
    internal class MyArray
    {
        private readonly Random _rng;
        protected int[] _array;
        public int _lenght;


        public MyArray()
        {
            _rng = new Random();
            SetTheLenghtArray();
        }

        public int this[int index]
        {
            get
            {
                if (index >= 0 && index < _array.Length)
                {
                    return _array[index];
                }
                Console.WriteLine("The index is outside the array!");
                return -1;
            }
            set { _array[index] = value; }
        }

        protected void SetTheLenghtArray()
        {
            var islenght = false;

            while (!islenght)
            {
                Console.Write("Please, enter the lenght of array: ");
                var userLenght = Convert.ToInt32(Console.ReadLine());
                if (userLenght > 0)
                {
                    _lenght = userLenght;
                    islenght = true;
                    Console.Clear();
                }
                else
                {
                    Console.WriteLine("Is invalid array length");
                }
            }

            // Console.WriteLine(lenght);
        }

        private void FillArrayManually()
        {
            Console.Clear();
            _array = new int[_lenght];
            Console.WriteLine("Please, enter array elements. The array lenght is: {0}.", _lenght);

            for (var i = 0; i < _lenght; i++)
            {
                Console.Write("Element {0}: ", i + 1);
                _array[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.Clear();
        }

        public void PrintArray()
        {
            Console.Clear();
            foreach (var elm in _array)
            {
                Console.Write(elm + " ");
            }
            Console.Write("\n");


        }

        private int[] FillArrayAutomatically()
        {
            _array = new int[_lenght];

            for (var i = 0; i < _lenght; i++)
            {
                _array[i] = _rng.Next(0, 10);
            }

            return _array;
        }

        public void CreateArray()
        {
            Console.WriteLine("To fill an array automatically, press 1 or press 2, to fill an array manually.");

            var key = Convert.ToInt32(Console.ReadLine());

            switch (key)
            {
                case 1:
                    {
                        FillArrayAutomatically();
                        Console.Clear();
                        Console.Write("Array is created.");
                        Console.ReadKey();
                        break;
                    }
                case 2:
                    {
                        FillArrayManually();
                        Console.Clear();
                        Console.Write("Array is created.");
                        Console.ReadKey();
                        break;
                    }

            }
        }
    }
}