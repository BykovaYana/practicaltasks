﻿using System;

namespace PracticalTasks.Arrays
{
    internal class GetMiddle : MyArray
    {
        private readonly int[] _newArray;
        private int indexMiddelFirstArray;
        private int indexMiddelSecondArray;

        public GetMiddle()
        {
            indexMiddelFirstArray = 0;
            indexMiddelSecondArray = 0;
            _newArray = new[] {0, 0};
            CheckTheLenght();
        }

        private void CheckTheLenght()
        {
            var isOddLenght = false;
            while (!isOddLenght)
            {
                SetTheLenghtArray();
                if (_lenght%2 != 0 && _lenght > 2)
                {
                    isOddLenght = true;
                    Console.Clear();
                }
                else
                {
                    Console.WriteLine("The lenght must be odd!");
                    Console.ReadKey();
                    Console.Clear();
                }
            }
        }

        public int[] GetMiddelArrays(GetMiddle firrstArray, GetMiddle secondArray)
        {
            indexMiddelFirstArray = firrstArray._lenght/2;
            indexMiddelSecondArray = secondArray._lenght/2;
            _newArray[0] = firrstArray._array[indexMiddelFirstArray];
            _newArray[1] = secondArray._array[indexMiddelSecondArray];
            return _newArray;
        }
    }
}