﻿using System;
using System.IO;

namespace PracticalTasks.Arrays
{
    internal class MakePi
    {
        private readonly int[] _array;
        private int _lenght;
        private string[] _pi;
        private string _piString;


        public MakePi()
        {
            GetTheLenghtArray();
            _array = new int[_lenght];
            ReadPi();
        }

        private void ReadPi()
        {
            _piString = File.ReadAllText(@"C:\Yana\CoursesPracticalTasks\PracticalTasks\Arrays\Pi.txt");
            // Console.WriteLine(_piString[1]);
            _pi = _piString.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
        }

        private void GetTheLenghtArray()
        {
            var islenght = false;

            while (!islenght)
            {
                Console.Write("Please, enter the lenght of array: ");
                var userLenght = Convert.ToInt32(Console.ReadLine());
                if (userLenght > 0)
                {
                    _lenght = userLenght;
                    islenght = true;
                    Console.Clear();
                }
                else
                {
                    Console.WriteLine("Is invalid array length");
                }
            }
        }

        public void CreateArrayPi()
        {
            for (var i = 0; i < _lenght; i++)
            {
                _array[i] = int.Parse(_pi[i]);
            }
        }

        public void PrintArray()
        {
            Console.Clear();
            foreach (var elm in _array)
            {
                Console.Write(elm + " ");
            }
        }
    }
}