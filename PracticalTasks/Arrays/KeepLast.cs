﻿namespace PracticalTasks.Arrays
{
    internal class KeepLast : MyArray
    {
        private readonly int[] newArray;
        private readonly int newLength;
        private int lastElement;

        public KeepLast()
        {
            CreateArray();
            lastElement = 0;
            newLength = _lenght * 2;
            newArray = new int[newLength];
        }

        public int[] KeepLastElement()
        {
            lastElement = _array[_lenght - 1];
            newArray[newLength - 1] = lastElement;
            return newArray;
        }
    }
}