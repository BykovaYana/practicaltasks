﻿using System;

namespace PracticalTasks.Arrays
{
    internal class Fix23 : MyArray
    {
        private bool is23;

        public Fix23()
        {
            is23 = false;
            CreateArray();
        }

        public void ChangeArray()
        {
            for (var i = 0; i < _lenght; i++)
            {
                if (_array[i] == 2 && _array[i + 1] == 3)
                {
                    _array[i + 1] = 0;
                    is23 = true;
                }
            }
            if (!is23)
            {
                Console.WriteLine("Not found in the array 2 and 3 in a row!");
            }
            else
            {
                PrintArray();
            }
        }
    }
}