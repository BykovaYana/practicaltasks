﻿using System;

namespace PracticalTasks.Arrays
{
    internal class Sum
    {
        private int sum;

        public Sum()
        {
            sum = 0;
        }

        public void SumElements(MyArray array)
        {
            for (var i = 0; i < array._lenght; i++)
            {
                sum += array[i];
            }

            Console.WriteLine("The sum of elements in the array is: {0}", sum);
        }
    }
}