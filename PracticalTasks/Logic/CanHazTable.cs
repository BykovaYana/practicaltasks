﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Logic
{
    class CanHazTable
    {
        private int result;

        public CanHazTable()
        {
            result = 0;
        }

        private int  GetStylishnessOfYourClothes()
        {
            int stylishnessOfYourClothes = -1;
            while (stylishnessOfYourClothes < 0 || stylishnessOfYourClothes > 10)
            {
                Console.Write("Enter an estimate of your dress code. In the range 0..10! ");
                stylishnessOfYourClothes = Convert.ToInt32(Console.ReadLine());
                if (stylishnessOfYourClothes < 0 || stylishnessOfYourClothes > 10)
                {
                    Console.WriteLine("The number is not in range 0..10! Try again!");
                }
            }          

            Console.WriteLine("Stylishness of your clothes is: {0} ", stylishnessOfYourClothes);

            return stylishnessOfYourClothes; 
         
        }

        private int GetStylishnessOfYourDatesClothes()
        {
            int stylishnessOfYourDatesClothes = -1;
            while (stylishnessOfYourDatesClothes < 0 || stylishnessOfYourDatesClothes > 10)
            {
                Console.Write("Enter an estimate of your  date`s dress code. In the range 0..10! ");
                stylishnessOfYourDatesClothes = Convert.ToInt32(Console.ReadLine());
                if (stylishnessOfYourDatesClothes < 0 || stylishnessOfYourDatesClothes > 10)
                {
                    Console.WriteLine("The number is not in range 0..10! Try again!");
                }
            }

            Console.WriteLine("Stylishness of your date`s clothes is: {0}", stylishnessOfYourDatesClothes);
            return stylishnessOfYourDatesClothes;

        }

        private int CheckResult(int stylishnessOfYourClothes,  int stylishnessOfYourDatesClothes)
        {
            if (stylishnessOfYourClothes >= 8 || stylishnessOfYourDatesClothes >= 8)
            {
                if (stylishnessOfYourClothes <= 2 || stylishnessOfYourDatesClothes <= 2)
                {
                    return 0;
                }
                return 2;
            }
            else if (stylishnessOfYourDatesClothes < 8 && stylishnessOfYourDatesClothes > 3)
            {
                if (stylishnessOfYourClothes < 8 && stylishnessOfYourClothes > 3)

                {
                    return 1;
                }
            }
            return 0;
        }

        public void ConvertIntToString(int value)
        {
            if (value == 0)
            {
                Console.WriteLine("NO!");
                return;
            }
            else if (value == 1)
            {
                Console.WriteLine("Maybe!");
                return;
            }
            Console.WriteLine("Yes!");
        }

        public void Run()
        {
            result = CheckResult(GetStylishnessOfYourClothes(), GetStylishnessOfYourDatesClothes());
            ConvertIntToString(result);
        }
    }
}
