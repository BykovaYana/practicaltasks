﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalTasks.Logic
{
    class GreatParty
    {
        private bool isWeekend;
        private int numberOfCigars;

        private bool ConvertStringToBool(string choice)
        {
            if (choice.Equals("Y", StringComparison.CurrentCultureIgnoreCase))
            {
                return true;
            }
            return false;
        }

        private void GetSettings()
        {
            Console.Write("How many cigars have squirrels? ");
            numberOfCigars = Convert.ToInt32(Console.ReadLine());

            var isValidChoice = false;
            string stringWeekend;

            while (!isValidChoice)
            {
                Console.WriteLine("Today weekend?");
                Console.Write("Yes - press Y, No - press N.\t");
                stringWeekend = Console.ReadLine();

                if (stringWeekend.Equals("Y", StringComparison.CurrentCultureIgnoreCase) ||
                    stringWeekend.Equals("N", StringComparison.CurrentCultureIgnoreCase))
                {
                    isWeekend = ConvertStringToBool(stringWeekend);
                    isValidChoice = true;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input. Try again.");
                }
            }

        }

        private bool CheckParty()
        {
            if (!isWeekend)
            {
                if (numberOfCigars >= 40 && numberOfCigars <= 60)
                {
                    Console.WriteLine("Party is  great!");
                    return true;
                }
                else
                {
                    Console.WriteLine("Party is not great!");
                    return false;
                }
            }
            Console.WriteLine("Party is  great!");
            return true;

        }
        public void Run()
        {
            GetSettings();
            Console.WriteLine(CheckParty());

        }
    }
}
